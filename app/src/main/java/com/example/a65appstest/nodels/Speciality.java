package com.example.a65appstest.nodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Speciality {
    @SerializedName("specialty_id")
    @Expose
    private  Integer Id;
    @SerializedName("name")
    @Expose
    private String Name;

    public Integer getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public void setName(String name) {
        Name = name;
    }
}
