package com.example.a65appstest.nodels;

import androidx.versionedparcelable.VersionedParcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Calendar;
import java.util.Locale;
import static java.util.Calendar.*;
import java.util.Date;

public class Worker {
    @SerializedName("f_name")
    @Expose
    private String firstName;
    @SerializedName("l_name")
    @Expose
    private String lastName;
    @SerializedName("birthday")
    @Expose
    private String birhtDate;
    @SerializedName("avatr_url")
    @Expose
    private String avatarUrl;
    @SerializedName("specialty")
    @Expose
    private List<Speciality> specialty;

    public List<Speciality> getSpecs(){return specialty;}

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public void setBirhtDate(String birhtDate) {
        this.birhtDate = birhtDate;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1).toLowerCase();
        return firstName;
    }

    public String getLastName() {
        lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1).toLowerCase();
        return lastName;
    }

    public String getBirhtDate() {
        if (birhtDate == null) {
            birhtDate = "-";
        }
        Date date = new Date();
        if (birhtDate.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")){
            try {
                date = new SimpleDateFormat("yyyy-MM-dd").parse(birhtDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String sdfStr = new SimpleDateFormat("dd.MM.yyyy").format(date);
            return sdfStr;
        } if (birhtDate.matches("([0-9]{2})-([0-9]{2})-([0-9]{4})")) {
            try {
                date = new SimpleDateFormat("dd-MM-yyyy").parse(birhtDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String sdfStr = new SimpleDateFormat("dd.MM.yyyy").format(date);
            return sdfStr;
        } else {
            return "";
        }
    }

    public Integer getWorkerAge(){
        if (birhtDate != null) {
            Date date = new Date();
            Date now = new Date();
            if (birhtDate.matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")) {
                try{
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(birhtDate);
                    Calendar a = getCalendar(date);
                    Calendar b = getCalendar(now);
                    int diff = b.get(YEAR) - a.get(YEAR);
                    if (a.get(MONTH) > b.get(MONTH) ||
                            (a.get(MONTH) == b.get(MONTH) && a.get(DATE) > b.get(DATE))) {
                        diff--;
                    }
                    return diff;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (birhtDate.matches("([0-9]{2})-([0-9]{2})-([0-9]{4})")) {
                try {
                    date = new SimpleDateFormat("dd-MM-yyyy").parse(birhtDate);
                    Calendar a = getCalendar(date);
                    Calendar b = getCalendar(now);
                    int diff = b.get(YEAR) - a.get(YEAR);
                    if (a.get(MONTH) > b.get(MONTH) ||
                            (a.get(MONTH) == b.get(MONTH) && a.get(DATE) > b.get(DATE))) {
                        diff--;
                    }
                    return diff;
                } catch (ParseException s) {
                    s.printStackTrace();
                }
            }
        }
        return 0;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }


    public String getAvatarUrl() {
        if (avatarUrl == null) {
            avatarUrl = "";
        }
        return avatarUrl;
    }

    public String getFullName(){
        String fullNameString = getFirstName() + " " + getLastName();
        return fullNameString;
    }
}
