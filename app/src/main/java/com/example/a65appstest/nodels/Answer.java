package com.example.a65appstest.nodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Answer {
    @SerializedName("response")
    @Expose
    private List<Worker> resData;

    public List<Worker> getResData() {
        return resData;
    }
}
