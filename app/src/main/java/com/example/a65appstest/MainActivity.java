package com.example.a65appstest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Application;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.a65appstest.adapter.WorkersListAdapter;
import com.example.a65appstest.api.NetworkService;
import com.example.a65appstest.fragments.SpecialityListFragment;
import com.example.a65appstest.fragments.WorkerDetailFragment;
import com.example.a65appstest.fragments.WorkersListFragment;
import com.example.a65appstest.nodels.Answer;
import com.example.a65appstest.nodels.Speciality;
import com.example.a65appstest.nodels.Worker;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SpecialityListFragment.onSpecialtySelected, WorkersListFragment.onWorkerSelected {

    List<Worker> workers = new ArrayList<>();
    List<Speciality> specs = new ArrayList<>();
    private ProgressBar spinner;
    FragmentTransaction fTrans;
    SpecialityListFragment specialityFragment;
    WorkersListFragment workersListFragment;
    WorkerDetailFragment workerDetailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);
        NetworkService.getInstance()
                .getJSONApi()
                .getWWorkersList()
                .enqueue(new Callback<Answer>(){
                    @Override
                    public void onResponse(@NonNull Call<Answer> call, @NonNull Response<Answer> response) {
                        List<Worker> answer = response.body().getResData();
                        workers = answer;
                        spinner.setVisibility(View.GONE);
                        Bundle b = new Bundle();
                        b.putString("specs", new Gson().toJson(getSpecsList()));
                        specialityFragment = new SpecialityListFragment();
                        specialityFragment.setArguments(b);
                        fTrans = getFragmentManager().beginTransaction();
                        fTrans.add(R.id.rootLayout, specialityFragment);
                        fTrans.commit();
                    }

                    @Override
                    public void onFailure(@NonNull Call<Answer> call, @NonNull Throwable t) {
                        spinner.setVisibility(View.GONE);
                        System.out.println("somthing get wrong");
                    }});
    }

    public List<Speciality> getSpecsList() {
        List<Integer> listSpecIds = new ArrayList<>();
        for (Worker worker :workers) {
            List<Speciality> workerspecs = worker.getSpecs();
            for (Speciality s : workerspecs){
                if (!listSpecIds.contains(s.getId())){
                    listSpecIds.add(s.getId());
                    specs.add(s);
                }
            }
        }
        return specs;
    }

    public  List<Worker> getWorkersFromSpec(Speciality spec) {
        List<Worker> selectedWorkers = new ArrayList<>();
        for (Worker worker: workers) {
            List<Speciality> workerspecs = worker.getSpecs();
            for (Speciality s : workerspecs){
                if (s.getId() == spec.getId()){
                    selectedWorkers.add(worker);
                }
            }
        }
        return selectedWorkers;
    }

    @Override
    public void specSelected(Speciality spec) {
        Bundle b = new Bundle();
        b.putString("workers", new Gson().toJson(getWorkersFromSpec(spec)));
        workersListFragment = new WorkersListFragment();
        workersListFragment.setArguments(b);
        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.rootLayout, workersListFragment);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }

    @Override
    public void workerSelected(Worker worker) {
        Bundle b = new Bundle();
        b.putString("worker", new Gson().toJson(worker));
        workerDetailFragment = new WorkerDetailFragment();
        workerDetailFragment.setArguments(b);
        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.rootLayout, workerDetailFragment);
        fTrans.addToBackStack(null);
        fTrans.commit();
    }
}
