package com.example.a65appstest.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.Fragment;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a65appstest.R;
import com.example.a65appstest.adapter.WorkersListAdapter;
import com.example.a65appstest.nodels.Speciality;
import com.example.a65appstest.nodels.Worker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class WorkersListFragment extends Fragment {

    public interface onWorkerSelected {
        void workerSelected(Worker worker);
    }
    onWorkerSelected workerSelected;

    List<Worker> workers = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            workerSelected = (onWorkerSelected) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        String workersStr = bundle.getString("workers");
        Type listType = new TypeToken<ArrayList<Worker>>(){}.getType();
        workers = new Gson().fromJson(workersStr, listType);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
       View v =  inflater.inflate(R.layout.fragment_workers_list, null);
       recyclerView = v.findViewById(R.id.workersListRecyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        WorkersListAdapter adapter = new WorkersListAdapter(getActivity(), workers);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener( new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                workerSelected.workerSelected(workers.get(position));
            }

            @Override
            public void onLongItemClick(View view, int position) {
                workerSelected.workerSelected(workers.get(position));
            }
        }));
       return v;
   }
}
