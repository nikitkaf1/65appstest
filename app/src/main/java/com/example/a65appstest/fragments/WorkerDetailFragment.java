package com.example.a65appstest.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.a65appstest.R;
import com.example.a65appstest.nodels.Worker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class WorkerDetailFragment extends Fragment {

    Worker worker = new Worker();

    private TextView firstName;
    private TextView lastName;
    private TextView birthDay;
    private ImageView avatar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        String workersStr = bundle.getString("worker");
        Type listType = new TypeToken<Worker>(){}.getType();
        worker = new Gson().fromJson(workersStr, listType);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_worker_detail, null);
        firstName = v.findViewById(R.id.firstname_textview);
        lastName = v.findViewById(R.id.lastname_textview);
        birthDay = v.findViewById(R.id.birthday_textview);
        avatar = v.findViewById(R.id.avatar_image_view);
        firstName.setText(worker.getFirstName());
        lastName.setText(worker.getLastName());
        birthDay.setText(worker.getBirhtDate());
        if (worker.getBirhtDate().isEmpty()) {
            birthDay.setText("-");
        }
        Glide.with(this).load(worker.getAvatarUrl()).into(avatar);
        if (worker.getAvatarUrl().isEmpty()) {
            avatar.setVisibility(View.GONE);
        }
        return v;
    }
}
