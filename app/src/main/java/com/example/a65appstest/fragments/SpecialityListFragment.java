package com.example.a65appstest.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.app.Fragment;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a65appstest.R;
import com.example.a65appstest.adapter.SpecialityListAdapter;
import com.example.a65appstest.nodels.Speciality;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class SpecialityListFragment extends Fragment {

    public interface onSpecialtySelected {
        void specSelected(Speciality spec);
    }

    onSpecialtySelected specialtySelected;

    List<Speciality> specs = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            specialtySelected = (onSpecialtySelected) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        String specStr = bundle.getString("specs");
        Type listType = new TypeToken<ArrayList<Speciality>>(){}.getType();
        specs = new Gson().fromJson(specStr, listType);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_speciality_list, null);
        recyclerView = v.findViewById(R.id.specialityListRecyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        SpecialityListAdapter adapter = new SpecialityListAdapter(getActivity(), specs);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener( new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                specialtySelected.specSelected(specs.get(position));
            }

            @Override
            public void onLongItemClick(View view, int position) {
                specialtySelected.specSelected(specs.get(position));
            }
        }));
        return v;
    }


}
