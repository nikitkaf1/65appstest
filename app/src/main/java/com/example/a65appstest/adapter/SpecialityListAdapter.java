package com.example.a65appstest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a65appstest.R;
import com.example.a65appstest.nodels.Speciality;

import java.util.ArrayList;
import java.util.List;

public class SpecialityListAdapter extends RecyclerView.Adapter<SpecialityListAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<Speciality> specsList;


    public SpecialityListAdapter(Context context, List<Speciality> specs){
        this.inflater = LayoutInflater.from(context);
        this.specsList = specs;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_speciality, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Speciality spec = specsList.get(position);
        holder.specialityName.setText(spec.getName());
    }

    @Override
    public int getItemCount() {
        return specsList.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        final TextView specialityName;

        ViewHolder(View view) {
            super(view);
            specialityName = view.findViewById(R.id.item_name_textView);
        }
    }
}
