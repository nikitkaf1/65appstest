package com.example.a65appstest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.a65appstest.R;
import com.example.a65appstest.nodels.Worker;
import java.util.List;

public class WorkersListAdapter extends RecyclerView.Adapter<WorkersListAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private List<Worker> workers ;

    public WorkersListAdapter(Context context, List<Worker> workers){
        this.workers  = workers;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_list_workers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Worker worker = workers.get(position);
        holder.workerName.setText(worker.getFullName());
        if (worker.getWorkerAge() > 0) {
            holder.workerAge.setText(worker.getWorkerAge().toString());
        } else {
            holder.workerAge.setText("-");
        }
    }

    @Override
    public int getItemCount() {
        return workers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

    final TextView workerName;
    final TextView workerAge;

    ViewHolder(View view){
        super(view);
        workerName = view.findViewById(R.id.worker_name_textView);
        workerAge = view.findViewById(R.id.worker_age_textView);
         }
    }
}
