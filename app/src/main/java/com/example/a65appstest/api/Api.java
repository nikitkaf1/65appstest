package com.example.a65appstest.api;

import com.example.a65appstest.nodels.Answer;
import com.example.a65appstest.nodels.Worker;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    @GET("/65gb/static/raw/master/testTask.json")
    public Call<Answer> getWWorkersList();
}
